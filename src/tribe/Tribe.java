package tribe;

public class Tribe {
    private String name;
    private boolean knowFire;
    private int count;

    public Tribe() {
    }

    public Tribe(String name, boolean knowFire, int count) {
        this.name = name;
        this.knowFire = knowFire;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isKnowFire() {
        return knowFire;
    }

    public void setKnowFire(boolean knowFire) {
        this.knowFire = knowFire;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
