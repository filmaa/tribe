package tribe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TribeController {

    private List<Tribe> savedList = new ArrayList<>();

    //    tests date, you should delete it
    {
        savedList.add(new Tribe("first", true, 789));
        savedList.add(new Tribe("second", false, 852));
        savedList.add(new Tribe("third", true, 7410));
    }


    // create new tribe
    public void addTribe() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter name of tribe: ");
        Tribe tribe = new Tribe();
        tribe.setName(scanner.next());
        System.out.println("does tribe know about fire?(yes/no)");
        while (true) {
            String isKnowFire = scanner.next();
            if (checkYesNo(isKnowFire)) {
                if (isKnowFire.equals("yes")) {
                    tribe.setKnowFire(true);
                    break;
                } else {
                    tribe.setKnowFire(false);
                    break;
                }
            } else {
                System.out.println("not correct input!");
            }
        }
        System.out.println("how many persons are there?");
        while (true) {
            String count = scanner.next();
            if (checkNumber(count)) {
                tribe.setCount(Integer.valueOf(count));
                savedList.add(tribe);
                break;
            } else {
                System.out.println("not correct value");
            }
        }
    }

    //    print in file
    public void printInFile() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File("count_know_fire.txt")))) {
            bw.write(countTribeKnow());
        } catch (IOException e) {
            System.out.println("something go wrong");
        }
    }

    //    count how much there are tribes
    private String countTribeKnow() {
        int know = 0;
        for (Tribe tribe : savedList) {
            if (tribe.isKnowFire()) {
                know++;
            }
        }
        return know + " tribes know about fire";
    }

    private boolean checkYesNo(String isKnowFire) {
        return isKnowFire.equals("yes") || isKnowFire.equals("no");
    }

    private boolean checkNumber(String quantity) {
        return quantity.matches("\\d*");
    }
}
